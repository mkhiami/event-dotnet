﻿using WebApplication1.Models;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Context
{
    public class FuegoDbContext : DbContext
    {
        public FuegoDbContext(DbContextOptions<FuegoDbContext> options)
            : base(options)
        {
        }

        public DbSet<LanguageModel> LanguageItems { get; set; }
        public DbSet<TranslationModel> TranslationItems { get; set; }
        public DbSet<MultiLangModel> MultiLangItems { get; set; }
        public DbSet<PropGroupModel> PropGroupItems { get; set; }
        public DbSet<PropModel> PropItems { get; set; }
        public DbSet<CredentialModel> CredentialItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LanguageModel>().ToTable("Language");
            modelBuilder.Entity<TranslationModel>().ToTable("Translation");
            modelBuilder.Entity<MultiLangModel>().ToTable("MultiLang");
            modelBuilder.Entity<PropGroupModel>().ToTable("PropGroup");
            modelBuilder.Entity<PropModel>().ToTable("Prop");
            modelBuilder.Entity<CredentialModel>().ToTable("Credential");
        }
    }
}
