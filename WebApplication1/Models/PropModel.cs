﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class PropModel
    {
        [Key]
        public int id { get; set; }

        [Required, MinLength(3), MaxLength(40)]
        public string name { get; set; }

        [Required, MinLength(3), MaxLength(40)]
        public string type { get; set; }

        [Required]
        public int propGroupId { get; set; }

        [Required]
        public byte isActive { get; set; }

        public DateTime dateCreated { get; set; }

        public DateTime dateModified { get; set; }
    }
}
