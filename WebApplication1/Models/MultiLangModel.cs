﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class MultiLangModel
    {
        [Key]
        public int id { get; set; }

        [Required, MinLength(2), MaxLength(40)]
        public string tag { get; set; }

        [Required, MinLength(2), MaxLength(40)]
        public string label { get; set; }

        public string description { get; set; }

        // Reference to Translation table
        public int translationId { get; set; }

        // Reference to Credential table
        public int credentialId { get; set; }

        // Reference to Prop table
        public int propId { get; set; }

        // Reference to PropGroup table
        public int propGroupId { get; set; } 
    }
}
