﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Context;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class TranslationController : Controller
    {
        private readonly FuegoDbContext _context;

        public TranslationController(FuegoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public JArray GetAll()
        {
            dynamic response = new JArray();

            var allItems = _context.TranslationItems.ToList();
            foreach (TranslationModel item in allItems)
            {
                dynamic resItem = new JObject();

                //Config response
                resItem._id = item.id;
                resItem.name = item.name;
                resItem.isActive = item.isActive == 1 ? true : false;
                resItem.dateCreated = item.dateCreated;
                resItem.dateModified = item.dateModified;
                // Handle MultiLang Part
                resItem.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.translationId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    resItem.local.Add(resML);
                }
                response.Add(resItem);
            }

            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpGet("{id}")]
        public JObject GetById(int id)
        {
            dynamic response = new JObject();

            var item = _context.TranslationItems.Find(id);
            if (item == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            //Config response
            response._id = item.id;
            response.name = item.name;
            response.isActive = item.isActive;
            response.dateCreated = item.dateCreated;
            response.dateModified = item.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            var multiLangItems = _context.MultiLangItems.Where(ml => ml.translationId == id);
            foreach (dynamic multiLang in multiLangItems)
            {
                dynamic resML = new JObject();
                resML._id = multiLang.id;
                resML.tag = multiLang.tag;
                resML.label = multiLang.label;
                resML.description = multiLang.description;
                response.local.Add(resML);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
            
        [HttpPost]
        public JObject Creat([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            TranslationModel newItem = new TranslationModel();
            // Get latest Id
            int maxId = _context.TranslationItems.LastOrDefault().id;
            // Config newItem
            newItem.id = maxId + 1;
            newItem.name = request.name;
            newItem.isActive = Convert.ToByte(request.isActive);
            newItem.dateCreated = DateTime.Now;
            newItem.dateModified = DateTime.Now;
            // Save newItem to DB
            _context.TranslationItems.Add(newItem);
            _context.SaveChanges();
            //Config response
            response._id = newItem.id;
            response.name = newItem.name;
            response.isActive = request.isActive;
            response.dateCreated = newItem.dateCreated;
            response.dateModified = newItem.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            MultiLangModel newMLItem = new MultiLangModel();
            foreach (dynamic multiLang in request.local)
            {
                // Save to DB
                int currentId = _context.MultiLangItems.LastOrDefault().id;
                newMLItem.id = currentId + 1;
                newMLItem.tag = multiLang.tag;
                newMLItem.label = multiLang.label;
                newMLItem.description = multiLang.description;
                newMLItem.translationId = maxId + 1;
                newMLItem.credentialId = -1;
                newMLItem.propId = -1;
                newMLItem.propGroupId = -1;
                _context.MultiLangItems.Add(newMLItem);
                _context.SaveChanges();
                // Handle response
                multiLang._id = currentId + 1;
                response.local.Add(multiLang);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPut("{id}")]
        public JObject Update([FromBody] JObject req, int id)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var updatedItem = _context.TranslationItems.Find(id);
            if (updatedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Config updatedItem
            updatedItem.name = request.name;
            updatedItem.isActive = Convert.ToByte(request.isActive);
            updatedItem.dateCreated = DateTime.Now;
            updatedItem.dateModified = DateTime.Now;
            // Save updatedItem to DB
            _context.TranslationItems.Update(updatedItem);
            _context.SaveChanges();
            //Config response
            response._id = id;
            response.name = updatedItem.name;
            response.isActive = request.isActive;
            response.dateCreated = updatedItem.dateCreated;
            response.dateModified = updatedItem.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            MultiLangModel updatedMLItem = new MultiLangModel();
            MultiLangModel newMLItem = new MultiLangModel();
            foreach (dynamic multiLang in request.local)
            {
                if(multiLang._id == null)
                {
                    // Save to DB
                    int currentId = _context.MultiLangItems.LastOrDefault().id;
                    newMLItem.id = currentId + 1;
                    newMLItem.tag = multiLang.tag;
                    newMLItem.label = multiLang.label;
                    newMLItem.description = multiLang.description;
                    newMLItem.translationId = id;
                    newMLItem.credentialId = -1;
                    newMLItem.propId = -1;
                    newMLItem.propGroupId = -1;
                    _context.MultiLangItems.Add(newMLItem);
                    _context.SaveChanges();
                    // Handle response
                    multiLang._id = currentId + 1;
                }
                else
                {
                    // Update to DB
                    updatedMLItem = _context.MultiLangItems.Find((int)multiLang._id);
                    updatedMLItem.tag = multiLang.tag;
                    updatedMLItem.label = multiLang.label;
                    updatedMLItem.description = multiLang.description;
                    _context.MultiLangItems.Update(updatedMLItem);
                    _context.SaveChanges();
                }
                // Handle response
                response.local.Add(multiLang);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpDelete("{id}")]
        public JObject Delete(int id)
        {
            dynamic response = new JObject();

            var deletedItem = _context.TranslationItems.Find(id);
            if (deletedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Remove deletedItem
            _context.TranslationItems.Remove(deletedItem);
            _context.SaveChanges();
            // Handle MultiLang Part
            var multiLangItems = _context.MultiLangItems.Where(ml => ml.translationId == id);
            foreach (MultiLangModel multiLang in multiLangItems)
            {
                var deletedMLItem = _context.MultiLangItems.Find(multiLang.id);
                _context.MultiLangItems.Remove(deletedMLItem);
            }
            _context.SaveChanges();
            //Config response
            response.msg = "deleted successfully.";
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}
