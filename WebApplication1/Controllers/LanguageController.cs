﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Context;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class LanguageController : Controller
    {
        private readonly FuegoDbContext _context;

        public LanguageController(FuegoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public JsonResult GetAll()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return Json(_context.LanguageItems.ToList());
        }

        [HttpGet("{id}")]
        public JsonResult GetById(int id)
        {
            var item = _context.LanguageItems.Find(id);
            if (item == null)
            {
                return Json("No result");
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return Json(item);
        }

        [HttpPost]
        public JObject Creat([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();
            
            LanguageModel newItem = new LanguageModel();
            // Get latest Id
            int maxId = _context.LanguageItems.LastOrDefault().id;
            // Config newItem
            newItem.id = maxId + 1;
            newItem.name = request.name;
            newItem.label = request.label;
            newItem.isActive = Convert.ToByte(request.isActive);
            newItem.dateCreated = DateTime.Now;
            newItem.dateModified = DateTime.Now;
            // Save newItem to DB
            _context.LanguageItems.Add(newItem);
            _context.SaveChanges();
            //Config response
            response._id = newItem.id;
            response.name = request.name;
            response.label = request.label;
            response.isActive = request.isActive;
            response.dateCreated = newItem.dateCreated;
            response.dateModified = newItem.dateModified;
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    
        [HttpPut("{id}")]
        public JObject Update([FromBody] JObject req, int id)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var updatedItem = _context.LanguageItems.Find(id);
            if (updatedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Config updatedItem
            updatedItem.name = request.name;
            updatedItem.label = request.label;
            updatedItem.isActive = Convert.ToByte(request.isActive);
            updatedItem.dateCreated = request.dateCreated;
            updatedItem.dateModified = request.dateModified;
            // Save updatedItem to DB
            _context.LanguageItems.Update(updatedItem);
            _context.SaveChanges();
            //Config response
            response._id = id;
            response.name = updatedItem.name;
            response.label = updatedItem.label;
            response.isActive = request.isActive;
            response.dateCreated = updatedItem.dateCreated;
            response.dateModified = updatedItem.dateModified;
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpDelete("{id}")]
        public JObject Delete(int id)
        {
            dynamic response = new JObject();

            var deletedItem = _context.LanguageItems.Find(id);
            if (deletedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Remove deletedItem
            _context.LanguageItems.Remove(deletedItem);
            _context.SaveChanges();
            //Config response
            response.msg = "deleted successfully.";
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}
