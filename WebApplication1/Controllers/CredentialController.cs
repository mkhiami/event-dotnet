﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Context;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class CredentialController : Controller
    {
        private readonly FuegoDbContext _context;

        public CredentialController(FuegoDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public JArray GetAll()
        {
            dynamic response = new JArray();

            var allItems = _context.CredentialItems.ToList();
            foreach (CredentialModel item in allItems)
            {
                dynamic resItem = new JObject();

                //Config response
                resItem._id = item.id;
                resItem.icon = item.icon;
                resItem.name = item.name;
                resItem.externalLink = item.externalLink;
                // START ----- Convert propId string from DB to array of int -----
                string[] stringId = (item.propId).Split(",");
                List<int> list = new List<int>();
                for (int ii = 0; ii < stringId.Length; ii++)
                {
                    list.Add(Convert.ToInt32(stringId[ii]));
                }
                var propIdJson = JsonConvert.SerializeObject(list);
                resItem.propId = propIdJson;
                // END   ----- Convert propId string from DB to array of int -----
                resItem.isActive = item.isActive == 1 ? true : false;
                resItem.dateCreated = item.dateCreated;
                resItem.dateModified = item.dateModified;
                // Handle MultiLang Part
                resItem.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.credentialId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    resItem.local.Add(resML);
                }
                response.Add(resItem);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpGet("{id}")]
        public JObject GetById(int id)
        {
            dynamic response = new JObject();

            var item = _context.CredentialItems.Find(id);
            if (item == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            //Config response
            response._id = item.id;
            response.icon = item.icon;
            response.name = item.name;
            response.externalLink = item.externalLink;
            // START ----- Convert propId string from DB to array of int -----
            string[] stringId = (item.propId).Split(",");
            List<int> list = new List<int>();
            for (int ii = 0; ii < stringId.Length; ii++)
            {
                list.Add(Convert.ToInt32(stringId[ii]));
            }
            var propIdJson = JsonConvert.SerializeObject(list);
            response.propId = propIdJson;
            // END   ----- Convert propId string from DB to array of int -----
            response.isActive = item.isActive;
            response.dateCreated = item.dateCreated;
            response.dateModified = item.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            var multiLangItems = _context.MultiLangItems.Where(ml => ml.credentialId == id);
            foreach (dynamic multiLang in multiLangItems)
            {
                dynamic resML = new JObject();
                resML._id = multiLang.id;
                resML.tag = multiLang.tag;
                resML.label = multiLang.label;
                resML.description = multiLang.description;
                response.local.Add(resML);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPost]
        public JObject Creat([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            CredentialModel newItem = new CredentialModel();
            // Get latest Id
            int maxId;
            if (_context.CredentialItems.LastOrDefault() == null)
            {
                maxId = 0;
            }
            else
            {
                maxId = _context.CredentialItems.LastOrDefault().id;
            }
            // Config newItem
            newItem.id = maxId + 1;
            newItem.icon = request.icon;
            newItem.name = request.name;
            newItem.externalLink = request.externalLink;
            var array_prop = new List<string>();
            foreach(dynamic item in request.props)
            {
                array_prop.Add((string)item.propId);
            }
            newItem.propId = string.Join(",", array_prop);
            newItem.isActive = Convert.ToByte(request.isActive);
            newItem.dateCreated = DateTime.Now;
            newItem.dateModified = DateTime.Now;
            // Save newItem to DB
            _context.CredentialItems.Add(newItem);
            _context.SaveChanges();
            //Config response
            response._id = newItem.id;
            response.icon = newItem.icon;
            response.name = newItem.name;
            response.externalLink = newItem.externalLink;
            response.props = request.props;
            response.isActive = request.isActive;
            response.dateCreated = newItem.dateCreated;
            response.dateModified = newItem.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            MultiLangModel newMLItem = new MultiLangModel();
            foreach (dynamic multiLang in request.local)
            {
                // Save to DB
                int currentId = _context.MultiLangItems.LastOrDefault().id;
                newMLItem.id = currentId + 1;
                newMLItem.tag = multiLang.tag;
                newMLItem.label = multiLang.label;
                newMLItem.description = multiLang.description;
                newMLItem.translationId = -1;
                newMLItem.credentialId = maxId + 1;
                newMLItem.propId = -1;
                newMLItem.propGroupId = -1;
                _context.MultiLangItems.Add(newMLItem);
                _context.SaveChanges();
                // Handle response
                multiLang._id = currentId + 1;
                response.local.Add(multiLang);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPut("{id}")]
        public JObject Update([FromBody] JObject req, int id)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var updatedItem = _context.CredentialItems.Find(id);
            if (updatedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Config updatedItem
            updatedItem.icon = request.icon;
            updatedItem.name = request.name;
            updatedItem.externalLink = request.externalLink;
            var array_prop = new List<string>();
            foreach (dynamic item in request.props)
            {
                array_prop.Add((string)item.propId);
            }
            updatedItem.propId = string.Join(",", array_prop);
            updatedItem.isActive = Convert.ToByte(request.isActive);
            updatedItem.dateCreated = DateTime.Now;
            updatedItem.dateModified = DateTime.Now;
            // Save updatedItem to DB
            _context.CredentialItems.Update(updatedItem);
            _context.SaveChanges();
            //Config response
            response._id = id;
            response.icon = updatedItem.icon;
            response.name = updatedItem.name;
            response.externalLink = updatedItem.externalLink;
            response.props = request.props;
            response.isActive = request.isActive;
            response.dateCreated = updatedItem.dateCreated;
            response.dateModified = updatedItem.dateModified;
            // Handle MultiLang Part
            response.local = new JArray();
            MultiLangModel updatedMLItem = new MultiLangModel();
            MultiLangModel newMLItem = new MultiLangModel();
            foreach (dynamic multiLang in request.local)
            {
                if (multiLang._id == null)
                {
                    // Save to DB
                    int currentId = _context.MultiLangItems.LastOrDefault().id;
                    newMLItem.id = currentId + 1;
                    newMLItem.tag = multiLang.tag;
                    newMLItem.label = multiLang.label;
                    newMLItem.description = multiLang.description;
                    newMLItem.translationId = -1;
                    newMLItem.credentialId = id;
                    newMLItem.propId = -1;
                    newMLItem.propGroupId = -1;
                    _context.MultiLangItems.Add(newMLItem);
                    _context.SaveChanges();
                    // Handle response
                    multiLang._id = currentId + 1;
                }
                else
                {
                    // Update to DB
                    updatedMLItem = _context.MultiLangItems.Find((int)multiLang._id);
                    updatedMLItem.tag = multiLang.tag;
                    updatedMLItem.label = multiLang.label;
                    updatedMLItem.description = multiLang.description;
                    _context.MultiLangItems.Update(updatedMLItem);
                    _context.SaveChanges();
                }
                // Handle response
                response.local.Add(multiLang);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpDelete("{id}")]
        public JObject Delete(int id)
        {
            dynamic response = new JObject();

            var deletedItem = _context.CredentialItems.Find(id);
            if (deletedItem == null)
            {
                dynamic result = new JObject();
                result.msg = "No Result";
                result.status = 404;
                return result;
            }
            // Remove deletedItem
            _context.CredentialItems.Remove(deletedItem);
            _context.SaveChanges();
            // Handle MultiLang Part
            var multiLangItems = _context.MultiLangItems.Where(ml => ml.credentialId == id);
            foreach (MultiLangModel multiLang in multiLangItems)
            {
                var deletedMLItem = _context.MultiLangItems.Find(multiLang.id);
                _context.MultiLangItems.Remove(deletedMLItem);
            }
            _context.SaveChanges();
            //Config response
            response.msg = "deleted successfully.";
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }
    }
}
