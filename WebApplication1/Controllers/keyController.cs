﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Context;
using System.Net.Http;
using System.Net;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class KeyController : Controller
    {
        private readonly FuegoDbContext _context;

        public KeyController(FuegoDbContext context)
        {
            _context = context;
        }
        
        [HttpPost("/api/keys/language")]
        public JObject KeyLanguage([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var total_data = _context.LanguageItems.ToList();
            var data = new List<LanguageModel>();
            if (request.sorting.dateCreated == 1)
            {
                data = _context.LanguageItems.OrderBy(s => s.dateCreated).ToList();
            }
            else
            {
                data = _context.LanguageItems.OrderByDescending(s => s.dateCreated).ToList();
            }
            //Config response
            response.total = total_data.Count;
            response.data = new JArray();
            foreach (LanguageModel item in data)
            {
                dynamic language = new JObject();
                language._id = item.id;
                language.name = item.name;
                language.label = item.label;
                language.isActive = item.isActive == 1 ? true : false;
                language.dateCreated = item.dateCreated;
                language.dateModified = item.dateModified;
                response.data.Add(language);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPost("/api/keys/translation")]
        public JObject KeyTranslation([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var total_data = _context.TranslationItems.ToList();
            var data = new List<TranslationModel>();
            if (request.sorting.dateCreated == 1)
            {
                data = _context.TranslationItems.OrderBy(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            } else
            {
                data = _context.TranslationItems.OrderByDescending(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            }
            
            //Config response
            response.total = total_data.Count;
            response.data = new JArray();
            foreach (TranslationModel item in data)
            {
                dynamic sub_item = new JObject();
                sub_item._id = item.id;
                sub_item.name = item.name;
                sub_item.isActive = item.isActive == 1 ? true : false;
                sub_item.dateCreated = item.dateCreated;
                sub_item.dateModified = item.dateModified;
                // Handle MultiLang Part
                sub_item.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.translationId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    sub_item.local.Add(resML);
                }
                response.data.Add(sub_item);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPost("/api/keys/prop-group")]
        public JObject KeyPropGroup([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var total_data = _context.PropGroupItems.ToList();
            var data = new List<PropGroupModel>();
            if (request.sorting.dateCreated == 1)
            {
                data = _context.PropGroupItems.OrderBy(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            }
            else
            {
                data = _context.PropGroupItems.OrderByDescending(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            }

            //Config response
            response.total = total_data.Count;
            response.data = new JArray();
            foreach (PropGroupModel item in data)
            {
                dynamic sub_item = new JObject();
                sub_item._id = item.id;
                sub_item.name = item.name;
                sub_item.isActive = item.isActive == 1 ? true : false;
                sub_item.dateCreated = item.dateCreated;
                sub_item.dateModified = item.dateModified;
                // Handle MultiLang Part
                sub_item.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.propGroupId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    sub_item.local.Add(resML);
                }
                response.data.Add(sub_item);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPost("/api/keys/prop")]
        public JObject KeyProp([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var total_data = _context.PropItems.ToList();
            var data = new List<PropModel>();
            if (request.sorting.dateCreated == 1)
            {
                data = _context.PropItems.OrderBy(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            }
            else
            {
                data = _context.PropItems.OrderByDescending(s => s.dateCreated).Skip((int)request.batchNumber * (int)request.batchSize).Take((int)request.batchSize).ToList();
            }

            //Config response
            response.total = total_data.Count;
            response.data = new JArray();
            foreach (PropModel item in data)
            {
                dynamic sub_item = new JObject();
                sub_item._id = item.id;
                sub_item.name = item.name;
                sub_item.type = item.type;                
                sub_item.isActive = item.isActive == 1 ? true : false;
                sub_item.dateCreated = item.dateCreated;
                sub_item.dateModified = item.dateModified;

                // Config toGroup
                dynamic toGroup = new JObject();
                var toGroupData = _context.PropGroupItems.Find(item.propGroupId);
                toGroup._id = item.propGroupId;
                toGroup.name = toGroupData.name;
                toGroup.isActive = toGroupData.isActive;
                toGroup.dateCreated = toGroupData.dateCreated;
                toGroup.dateModified = toGroupData.dateModified;
                toGroup.local = new JArray();
                var multiLangItemsForToGroup = _context.MultiLangItems.Where(ml => ml.propGroupId == item.propGroupId);
                foreach (dynamic multiLangForToGroup in multiLangItemsForToGroup)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLangForToGroup.id;
                    resML.tag = multiLangForToGroup.tag;
                    resML.label = multiLangForToGroup.label;
                    resML.description = multiLangForToGroup.description;
                    toGroup.local.Add(resML);
                }

                sub_item.toGroup = toGroup;

                // Handle MultiLang Part
                sub_item.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.propId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    sub_item.local.Add(resML);
                }
                response.data.Add(sub_item);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }

        [HttpPost("/api/keys/credential")]
        public JObject KeyCredential([FromBody] JObject req)
        {
            dynamic request = req;
            dynamic response = new JObject();

            var total_data = _context.CredentialItems.ToList();
            var data = new List<CredentialModel>();
            if (request.sorting.dateModified == 1)
            {
                data = _context.CredentialItems.OrderBy(s => s.dateModified).ToList();
            }
            else
            {
                data = _context.CredentialItems.OrderByDescending(s => s.dateModified).ToList();
            }

            //Config response
            response.total = total_data.Count;
            response.data = new JArray();
            foreach (CredentialModel item in data)
            {
                dynamic sub_item = new JObject();
                sub_item._id = item.id;
                sub_item.name = item.name;
                sub_item.icon = item.icon;
                sub_item.externalLink = item.externalLink;
                sub_item.isActive = item.isActive == 1 ? true : false;
                sub_item.dateCreated = item.dateCreated;
                sub_item.dateModified = item.dateModified;

                // Config props
                sub_item.props = new JArray();
                // START ----- Convert propId string from DB to array of int -----
                string[] stringId = (item.propId).Split(",");
                List<int> list = new List<int>();
                for (int ii = 0; ii < stringId.Length; ii++)
                {
                    list.Add(Convert.ToInt32(stringId[ii]));
                }
                // END   ----- Convert propId string from DB to array of int -----
                for (int i = 0; i < list.Count; i ++)
                {
                    dynamic prop_item = new JObject();
                    dynamic prop_item_propId = new JObject();
                    prop_item_propId._id = list[i];
                    prop_item.propId = prop_item_propId;
                    prop_item.required = true;
                    sub_item.props.Add(prop_item);
                }

                // Handle MultiLang Part
                sub_item.local = new JArray();
                var multiLangItems = _context.MultiLangItems.Where(ml => ml.credentialId == item.id);
                foreach (dynamic multiLang in multiLangItems)
                {
                    dynamic resML = new JObject();
                    resML._id = multiLang.id;
                    resML.tag = multiLang.tag;
                    resML.label = multiLang.label;
                    resML.description = multiLang.description;
                    sub_item.local.Add(resML);
                }
                response.data.Add(sub_item);
            }
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return response;
        }


        // OPTIONS
        [HttpOptions("/api/keys/prop")]
        public IActionResult Options()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true"); 
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/keys/prop-group")]
        public IActionResult Options1()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/keys/credential")]
        public IActionResult Options2()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/keys/translation")]
        public IActionResult Options3()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/keys/language")]
        public IActionResult Options4()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/prop")]
        public IActionResult Options5()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/prop-group")]
        public IActionResult Options6()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/credential")]
        public IActionResult Options7()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/translation")]
        public IActionResult Options8()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
        [HttpOptions("/api/language")]
        public IActionResult Options9()
        {
            HttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Origin,X-Requested-With,Content-Type,Accept");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            HttpContext.Response.Headers.Add("Allow", "GET,POST,DELETE,PUT");
            return Ok();
        }
    }
}
